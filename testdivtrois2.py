from sys import stderr


def div_by_three():
    while (num := input('Enter a number (END to leave): ')):    # une boucle pour lire les entrés d'utilisateur et le stoquer dans num
        if 'end' in num.lower(): #num.lower() convertir les lettre en miniscule, et vérifier si c'est égal à end
            print('Bye') # si la condition est respectée imprime Bye
            break # si la condition est respecté  arrete le boucle
        else: # sinon
            try: # pour attraper les erreurs
                num = int(num) #  convertir la valeur en int
            except ValueError: # s'il y a une erreur
                print('Not a whole number!', file=stderr) # affiche
                continue # continue la boucle

        if num % 3 == 0: # pour trouver s'il n'y a pas des  franction ==> par exemple 3%2=1   2%2=0
            digits = [int(d) for d in str(num)] # recuperer les chiffre de valeur dans la variable d et le convertir de nouveau en int
            print(f'Yes! The number {num} is divisble by 3.') # affiche le numéro
            print(f'Yes! The sum {sum(digits)} of digits {digits} is divisble by 3.')
        else: # s'il y a des fraction
            digits = [int(d) for d in str(num)]
            print(f'No! The number {num} is not divisible by 3.')
            print(f'No! The sum {sum(digits)} of digits {digits} is not divisble by 3.')


def proof():
    print('_' * 190, '\n', 'THEOREM'.center(150, ' ')) #imprimer
    print('A number N divisible by 3 and also sum of its numbers in base 10 is divisible by 3 and, conversely, if the sum of its figures is divisible by 3 then it is also divisible.')
    print('_' * 190)

    num = input('Enter a whole number: ') # recuperer des entrées et le stoquer dans num

    try: # pour attraper les erreurs
        num = int(num)  # convertir la valeur
    except ValueError: # s'il y a une erruer
        print('Not a number!', file=stderr)
        return # arret l'execution

    lstr = ''
    rlstr = ''
    rstr = ''
    rrstr = ''
    if num % 3 == 0: # pour trouver s'il n'y a pas des  fractions ==> par exemple 3%2=1   2%2=0
        print('PROOF'.center(50, ' ')) # imprime
        for i, p in enumerate(reversed([d for d in str(num)])):
            lstr += f'({p}x{10 ** i})+'
        for s in reversed(lstr[:-1].split('+')):
            rlstr += f'{s}+'

        rlstr = rlstr[:-1]

        for i, p in enumerate(reversed([d for d in str(num)])):  # enumerate => https://book.pythontips.com/en/latest/enumerate.html  ==> reversed pour revrcer l'ordre
            if int(i): # si la valeur est int je pense
                rstr += ('(' + ('9' * i) + '|1)' + 'x' + p + '+')
            else:
                rstr += p + '+'

        for s in reversed(rstr[:-1].split('+')):  # split separer
            rrstr += f'{s}+'

        rrstr = rrstr[:-1]

        print(rlstr, ' = ', rrstr.replace('|', '+')) # replace => remplacer

        lstr = ''
        for expr in rlstr.split('+'):
            lstr += str(eval(expr.replace('x', '*'))) + '+'

        lstr = lstr[:-1]

        rstr = ''
        for expr in rrstr.split('+'):
            rstr += str(eval(expr.replace('|', '+').replace('x', '*'))) + '+'
        rstr = rstr[:-1]

        print(lstr, ' = ', rstr.replace('|', '+'))
        lstr = eval(lstr)
        rstr = eval(rstr)
        print(lstr, ' = ', rstr)
    else:
        print('Number is not divisible by 3')

if __name__ == '__main__':

    div_by_three()
    proof()
