from string import ascii_lowercase
def main():
    data = list()
    while (u_in := input('Enter a food and a price (END to leave):')):
        if 'end' in u_in.lower():
            break

        item, price = None, None
        try:
            item, price = u_in.split(' ')
            price = float(price)
        except ValueError:
            if price:
                if price[0].lower() in ascii_lowercase:
                    print('The price is not a number')
            else:
                print('Invalid input! Try again...')
            continue

        data.append(
            (item, price)
        )

    print(f'You asked: {" ".join(map(lambda i: i[0], data))}')
    print(f'Total price is: {sum(map(lambda i: i[1], data))}')


if __name__ == '__main__':
    main()
